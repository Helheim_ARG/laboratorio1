Proceso estimado_de_pi
	definir x,num,resto,i Como Entero;
	
	definir acumulador, resultado Como Real;
	
	acumulador<-1;
	
	num<-3;
	
	resultado<-0;
	
	escribir "ingrese cantidad de veces que el numero que se aproxime a pi:";
	
	leer x;
	
	para i<-0 hasta x con paso 1 hacer 
		
		resto <- (i+1) mod 2;
		
		si resto = 0 entonces          
			
			acumulador<-acumulador +1/num;
			
			num<-num+2;
			
		SiNo acumulador<-acumulador -1/num;
			
			num<-num+2;
			
		FinSi
		
	FinPara
	
	resultado<-acumulador *4;
	
	escribir "resultado de las aproximaciones: ", resultado;
	
FinProceso
