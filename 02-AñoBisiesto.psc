Proceso Bisiesto
	Definir anio Como Entero;
	
	Definir Divisible4, Divisible100, Divisible400 Como Logico;
	
	Escribir "ingrese un a�o";
	Leer anio;
	
	Divisible4 <- anio mod 4 = 0;
	
	Divisible100 <- anio mod 100 = 0;
	
	Divisible400 <- anio mod 400 = 0;
	
	si(Divisible4 = Verdadero y Divisible100 = Falso) o Divisible400 = Verdadero Entonces;
		Escribir "el a�o es bisiesto";
		
	SiNo
		
		Escribir "el a�o no es bisiesto";
		
	FinSi
FinProceso
