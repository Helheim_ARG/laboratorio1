Proceso Calificacion
	
	Definir NotaDelAlumno Como Entero;
	
	Escribir "Ingrese su nota de 6 al 10";
	Leer NotaDelAlumno;
	
	Segun NotaDelAlumno Hacer
		10:
			Escribir "A-Exelente";
		9:
			Escribir "B-Buena";
		7:
			Escribir "C-Regular";
		6:
			Escribir "D-Suficiente";
		1,2,3,4,5:
			Escribir "F-No Suficiente";
	FinSegun
FinProceso
