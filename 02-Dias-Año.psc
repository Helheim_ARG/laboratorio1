SubProceso es_bisiesto <- bisiesto ( anio )
	Definir es_bisiesto, Divisible4, Divisible100, Divisible400 Como Logico;
	
	Divisible4 <- anio mod 4 = 0;
	
	Divisible100 <- anio mod 100 = 0;
	
	Divisible400 <- anio mod 400 = 0;
	
	si(Divisible4 = Verdadero y Divisible100 = Falso) o Divisible400 = Verdadero Entonces;
		
		es_bisiesto<- Verdadero;
		
	SiNo
		
		es_bisiesto<- Falso;
		
	FinSi
	
FinSubProceso



Proceso dias_anio
	Definir mes, anio Como Entero;
	
	Escribir "ingrese mes del a�o";
	Leer mes;
	Segun mes Hacer
		1,3,5,7,8,10,12:
			Escribir"31 dias";
		2:
			Escribir"ingrese a�o";
			Leer anio;
			si bisiesto(anio)= Verdadero Entonces 
				Escribir "29 dias";
			SiNo
				Escribir "28 dias";
			FinSi
			
		4,6,9,11:
			Escribir"30 dias";
		
		De Otro Modo:
			Escribir"Mes no valido";
	FinSegun
	
FinProceso
